
import networkx as nx
import random as rd

def node_in_community(n, list_comm):
    
    node_in_comm = [[] for i in range(n)]
    no_comm = list(range(n))
    
    for i_c, comm in enumerate(list_comm):
        for u in comm:
            node_in_comm[u].append(i_c)
            try:
                no_comm.remove(u)
            except:
                "pass"
    
    return node_in_comm, no_comm
                
                

def rd_disjoint_community(n, nb_comm, size_comm_min, size_comm_max):
    vertices = list(range(n))
    
    list_comm = []
    
    rd.shuffle(vertices)
    
    for i_c in range(nb_comm):
        comm_size = rd.randrange(size_comm_min, size_comm_max+1)
        comm = vertices[:comm_size]
        list_comm.append(comm)

        vertices = vertices[comm_size:]
    
    return list_comm


def rd_community(n, nb_comm, size_comm_min, size_comm_max):
    
    prob_community_intersect = 0.01
    prob_common_node = 0.05
    
    list_comm = rd_disjoint_community(n, nb_comm, size_comm_min, size_comm_max)
    
    for i_c in range(1,nb_comm):
        for j_c in range(i_c):
            
            if rd.random() < prob_community_intersect:
                
                size_comm_ic = len(list_comm[i_c])
                size_comm_jc = len(list_comm[j_c])
                
                for i in range(size_comm_ic):
                    if rd.random() < prob_common_node:
                        u = list_comm[i_c][i]
                        list_comm[j_c].append(u)
                
                for i in range(size_comm_jc):
                    if rd.random() < prob_common_node:
                        u = list_comm[j_c][i]
                        list_comm[i_c].append(u)
    
    return list_comm

def rd_community_graph(n, nb_comm, size_comm_min, size_comm_max, prob_edge_comm=0.9, prob_edge_inter_comm=0.1):
    
    G = nx.Graph()
    G.add_nodes_from(range(n))
    
    list_comm = rd_community(n, nb_comm, size_comm_min, size_comm_max)
    
    node_in_comm, no_comm = node_in_community(n, list_comm)
    
    for i_c in range(len(list_comm)):
        
        for i in range(1,len(list_comm[i_c])):
            for j in range(i):
                
                u = list_comm[i_c][i]
                v = list_comm[i_c][j]
                
                if (set(range(i_c)) & set(node_in_comm[u]) & set(node_in_comm[v])) == set():
                    if rd.random() < prob_edge_comm:
                        G.add_edge(u,v)
    
    nb_edges_outside_comm = 0
    
    for u in range(1,n):
        for v in range(u):
            if (set(node_in_comm[u]) & set(node_in_comm[v])) == set():
                if rd.random() < prob_edge_inter_comm:
                    G.add_edge(u,v)
                    nb_edges_outside_comm += 1
                    
    return G


def modify_seed(s):
    global seed
    seed = s
    rd.seed(s)

seed = 10
    
    
    
    
    
    
    

