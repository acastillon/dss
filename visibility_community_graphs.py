
import numpy as np
import matplotlib.pyplot as plt

def lire_fichier_donnees(filename):
    file = open(filename, 'r')
    
    file.readline().rstrip('\n\r')  #on oublie les 2 premieres lignes
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    nb_ligne = 0
    
    ligne = file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    vis_del_edges_true = 0
    vis_del_edges_false = 0
    
    vis_red_aware_true = 0
    vis_red_aware_false = 0
    
    vis_greedy_true = 0
    vis_greedy_false = 0
    
    while ligne:
        nb_ligne += 1
        l = ligne.split()
        
        #print(l)
        
        vis_del_edges_true += float(l[1][0:-1])
        vis_del_edges_false += float(l[3][0:-1])
        
        vis_red_aware_true += float(l[5][0:-1])
        vis_red_aware_false += float(l[7][0:-1])
        
        vis_greedy_true += float(l[9][0:-1])
        vis_greedy_false += float(l[11][0:-1])
        
        
        ligne = file.readline().rstrip('\n\r')
        file.readline().rstrip('\n\r')
    
    file.close()
    
    return np.array([vis_del_edges_true,vis_del_edges_false,vis_red_aware_true,vis_red_aware_false,vis_greedy_true,vis_greedy_false])/nb_ligne



vis_del_edges_true = []
vis_del_edges_false = []

vis_red_aware_true = []
vis_red_aware_false = []

vis_greedy_true = []
vis_greedy_false = []

x_max = 45

for i in range(x_max):
    l = lire_fichier_donnees("results\\Community_graphs\\community_graphs_vis_"+str(i)+".txt")
    vis_del_edges_true.append(l[0])
    vis_del_edges_false.append(l[1])
    
    vis_red_aware_true.append(l[2])
    vis_red_aware_false.append(l[3])
    
    vis_greedy_true.append(l[4])
    vis_greedy_false.append(l[5])



x = [0.001*i for i in range(x_max)]

text_size = 15
avec_legend = False

plt.figure()
plt.title("Visibility with pre-processing",fontsize=text_size)
plt.plot(x,[1 for i in range(x_max)],color="red",label="Quick")
plt.plot(x,vis_red_aware_true,color="blue",label="Quick_redundancy_aware")
plt.plot(x,vis_del_edges_true,color="green",label="Quick_delete_covered_edges")
plt.plot(x,vis_greedy_true,color="orange",label="Greedy_quasi_cliques")
axes = plt.gca()
axes.set_ylim(0,1.1)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\all_with_vis.png",format='png')


plt.figure()
plt.title("Visibility without pre-processing",fontsize=text_size)
plt.plot(x,[1 for i in range(x_max)],color="red",label="Quick")
plt.plot(x,vis_red_aware_false,color="blue",label="Quick_redundancy_aware")
plt.plot(x,vis_del_edges_false,color="green",label="Quick_delete_covered_edges")
plt.plot(x,vis_greedy_false,color="orange",label="Greedy_quasi_cliques")
axes = plt.gca()
axes.set_ylim(0,1.1)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\all_without_vis.png",format='png')

plt.figure()
plt.title("Quick_redundancy_aware visibility",fontsize=text_size)
plt.plot(x,[1 for i in range(x_max)],color="red",label="with pre-processing")
plt.plot(x,[1 for i in range(x_max)],color="blue",label="without pre-processing")
axes = plt.gca()
axes.set_ylim(0,1.1)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\quick_vis.png",format='png')

plt.figure()
plt.title("Quick_redundancy_aware visibility",fontsize=text_size)
plt.plot(x,vis_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,vis_red_aware_false,color="blue",label="without pre-processing")
axes = plt.gca()
axes.set_ylim(0,1.1)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\red_aware_vis.png",format='png')

plt.figure()
plt.title("Quick_delete_covered_edges visibility",fontsize=text_size)
plt.plot(x,vis_del_edges_true,color="red",label="with pre-processing")
plt.plot(x,vis_del_edges_false,color="blue",label="without pre-processing")
axes = plt.gca()
axes.set_ylim(0,1.1)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\del_edges_vis.png",format='png')

plt.figure()
plt.title("Greedy_quasi_cliques visibility",fontsize=text_size)
plt.plot(x,vis_greedy_true,color="red",label="with pre-processing")
plt.plot(x,vis_greedy_false,color="blue",label="without pre-processing")
axes = plt.gca()
axes.set_ylim(0,1.1)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\greedy_vis.png",format='png')

