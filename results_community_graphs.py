
import numpy as np
import matplotlib.pyplot as plt

def lire_fichier_donnees(filename):
    file = open(filename, 'r')
    
    file.readline().rstrip('\n\r')  #on oublie les 2 premieres lignes
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    nb_ligne = 0
    
    ligne = file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    t_baseline_true = 0
    comp_baseline_true = 0
    t_red_aware_true = 0
    comp_red_aware_true = 0
    t_del_edge_true = 0
    comp_del_edge_true = 0
    t_greedy_true = 0
    comp_greedy_true = 0
    t_baseline_false = 0
    comp_baseline_false = 0
    t_red_aware_false = 0
    comp_red_aware_false = 0
    t_del_edge_false = 0
    comp_del_edge_false = 0
    t_greedy_false = 0
    comp_greedy_false = 0
    
    while ligne:
        nb_ligne += 1
        l = ligne.split()
        
        t_baseline_true += float(l[0])
        comp_baseline_true += float(l[1])
        t_baseline_false += float(l[2])
        comp_baseline_false += float(l[3])
        t_del_edge_true += float(l[4])
        comp_del_edge_true += float(l[5])
        t_del_edge_false += float(l[6])
        comp_del_edge_false += float(l[7])
        t_red_aware_true += float(l[8])
        comp_red_aware_true += float(l[9])
        t_red_aware_false += float(l[10])
        comp_red_aware_false += float(l[11])
        t_greedy_true += float(l[12])
        comp_greedy_true += float(l[13])
        t_greedy_false += float(l[14])
        comp_greedy_false += float(l[15])
        
        
        ligne = file.readline().rstrip('\n\r')
        file.readline().rstrip('\n\r')
    
    file.close()
    
    return np.array([t_baseline_true, comp_baseline_true, t_baseline_false, comp_baseline_false, t_del_edge_true, comp_del_edge_true, t_del_edge_false, comp_del_edge_false, t_red_aware_true, comp_red_aware_true, t_red_aware_false, comp_red_aware_false, t_greedy_true, comp_greedy_true, t_greedy_false, comp_greedy_false])/nb_ligne



t_baseline_true = []
comp_baseline_true = []
t_baseline_false = []
comp_baseline_false = []
t_del_edge_true = []
comp_del_edge_true = []
t_del_edge_false = []
comp_del_edge_false = []
t_red_aware_true = []
comp_red_aware_true = []
t_red_aware_false = []
comp_red_aware_false = []
t_greedy_true = []
comp_greedy_true = []
t_greedy_false = []
comp_greedy_false = []

x_max = 45

for i in range(x_max):
    l = lire_fichier_donnees("results\\Community_graphs\\community_graphs_"+str(i)+".txt")
    t_baseline_true.append(l[0])
    comp_baseline_true.append(l[1])
    t_baseline_false.append(l[2])
    comp_baseline_false.append(l[3])
    t_del_edge_true.append(l[4])
    comp_del_edge_true.append(l[5])
    t_del_edge_false.append(l[6])
    comp_del_edge_false.append(l[7])
    t_red_aware_true.append(l[8])
    comp_red_aware_true.append(l[9])
    t_red_aware_false.append(l[10])
    comp_red_aware_false.append(l[11])
    t_greedy_true.append(l[12])
    comp_greedy_true.append(l[13])
    t_greedy_false.append(l[14])
    comp_greedy_false.append(l[15])

x = [0.001*i for i in range(x_max)]

text_size = 15
avec_legend = False

plt.figure()
plt.title("Runtime with pre-processing (in s)",fontsize=text_size)
plt.plot(x,t_baseline_true,color="red",label="Quick")
plt.plot(x,t_red_aware_true,color="blue",label="Quick_redundancy_aware")
plt.plot(x,t_del_edge_true,color="green",label="Quick_delete_covered_edges")
plt.plot(x,t_greedy_true,color="orange",label="Greedy_quasi_cliques")
#plt.xlabel(chr(946),fontsize=30)
#plt.ylabel("runtime",fontsize=30)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\all_with_time.png",format='png')

plt.figure()
plt.title("Runtime without pre-processing (in s)",fontsize=text_size)
plt.plot(x,t_baseline_false,color="red",label="quick")
plt.plot(x,t_red_aware_false,color="blue",label="red_aware")
plt.plot(x,t_del_edge_false,color="green",label="del_edges")
plt.plot(x,t_greedy_false,color="orange",label="greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\all_without_time.png",format='png')


plt.figure()
plt.title("Quick runtime (in s)",fontsize=text_size)
plt.plot(x,t_baseline_true,color="red",label="with pre-processing")
plt.plot(x,t_baseline_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\quick_time.png",format='png')

plt.figure()
plt.title("Quick_redundancy_aware runtime (in s)",fontsize=text_size)
plt.plot(x,t_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,t_red_aware_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\red_aware_time.png",format='png')

plt.figure()
plt.title("Quick_delete_covered_edges runtime (in s)",fontsize=text_size)
plt.plot(x,t_del_edge_true,color="red",label="with pre-processing")
plt.plot(x,t_del_edge_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\del_edges_time.png",format='png')

plt.figure()
plt.title("Greedy_quasi_cliques runtime (in s)",fontsize=text_size)
plt.plot(x,t_greedy_true,color="red",label="with pre-processing")
plt.plot(x,t_greedy_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\greedy_time.png",format='png')


plt.figure()
plt.title("Summary size with pre-processing",fontsize=text_size)
plt.plot(x,comp_baseline_true,color="red",label="quick")
plt.plot(x,comp_red_aware_true,color="blue",label="red_aware")
plt.plot(x,comp_del_edge_true,color="green",label="del_edges")
plt.plot(x,comp_greedy_true,color="orange",label="greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\all_with_size.png",format='png')

plt.figure()
plt.title("Summary size without pre-processing",fontsize=text_size)
plt.plot(x,comp_baseline_false,color="red",label="Quick")
plt.plot(x,comp_red_aware_false,color="blue",label="Redundancy aware")
plt.plot(x,comp_del_edge_false,color="green",label="Delete covered edges")
plt.plot(x,comp_greedy_false,color="orange",label="Greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\all_without_size.png",format='png')


plt.figure()
plt.title("Summary size with Quick",fontsize=text_size)
plt.plot(x,comp_baseline_true,color="red",label="with pre-processing")
plt.plot(x,comp_baseline_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\quick_size.png",format='png')

plt.figure()
plt.title("Summary size with Quick_redundancy_aware",fontsize=text_size)
plt.plot(x,comp_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,comp_red_aware_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\red_aware_size.png",format='png')

plt.figure()
plt.title("Summary size with Quick_delete_covered_edges",fontsize=text_size)
plt.plot(x,comp_del_edge_true,color="red",label="with pre-processing")
plt.plot(x,comp_del_edge_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\del_edges_size.png",format='png')

plt.figure()
plt.title("Summary size with Greedy_quasi_cliques",fontsize=text_size)
plt.plot(x,comp_greedy_true,color="red",label="with pre-processing")
plt.plot(x,comp_greedy_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Community_graphs\\greedy_size.png",format='png')






