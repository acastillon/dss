
import numpy as np
import matplotlib.pyplot as plt

def lire_fichier_donnees(filename):
    file = open(filename, 'r')
    
    file.readline().rstrip('\n\r')  #on oublie les 2 premieres lignes
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    nb_ligne = 0
    
    ligne = file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    moy_vis_quick_true = []
    min_vis_quick_true = []
    moy_vis_quick_false = []
    min_vis_quick_false = []
    moy_vis_del_true = []
    min_vis_del_true = []
    moy_vis_del_false = []
    min_vis_del_false = []
    moy_vis_red_true = []
    min_vis_red_true = []
    moy_vis_red_false = []
    min_vis_red_false = []
    moy_vis_greed_true = []
    min_vis_greed_true = []
    moy_vis_greed_false = []
    min_vis_greed_false = []
    x = []

    
    while ligne:
        nb_ligne += 1
        l = ligne.split()
        
        moy_vis_quick_true.append(float(l[0]))
        min_vis_quick_true.append(float(l[1]))
        moy_vis_quick_false.append(float(l[2]))
        min_vis_quick_false.append(float(l[3]))
        moy_vis_del_true.append(float(l[4]))
        min_vis_del_true.append(float(l[5]))
        moy_vis_del_false.append(float(l[6]))
        min_vis_del_false.append(float(l[7]))
        moy_vis_red_true.append(float(l[8]))
        min_vis_red_true.append(float(l[9]))
        moy_vis_red_false.append(float(l[10]))
        min_vis_red_false.append(float(l[11]))
        moy_vis_greed_true.append(float(l[12]))
        min_vis_greed_true.append(float(l[13]))
        moy_vis_greed_false.append(float(l[14]))
        min_vis_greed_false.append(float(l[15]))
        x.append(float(l[17]))
        
        ligne = file.readline().rstrip('\n\r')
        file.readline().rstrip('\n\r')
    
    file.close()
    
    return moy_vis_quick_true, min_vis_quick_true, moy_vis_quick_false, min_vis_quick_false, moy_vis_del_true, min_vis_del_true, moy_vis_del_false, min_vis_del_false, moy_vis_red_true, min_vis_red_true, moy_vis_red_false, min_vis_red_false, moy_vis_greed_true, min_vis_greed_true, moy_vis_greed_false, min_vis_greed_false, x



moy_vis_quick_true, min_vis_quick_true, moy_vis_quick_false, min_vis_quick_false, moy_vis_del_true, min_vis_del_true, moy_vis_del_false, min_vis_del_false, moy_vis_red_true, min_vis_red_true, moy_vis_red_false, min_vis_red_false, moy_vis_greed_true, min_vis_greed_true, moy_vis_greed_false, min_vis_greed_false, x = lire_fichier_donnees("resultats\\visibilite_ss_gphe_fb3.txt")

plt.figure()
plt.title("Visibility with pre-processing")
plt.plot(x,min_vis_quick_true,color="red",label="quick")
plt.plot(x,min_vis_red_true,color="blue",label="red_aware")
plt.plot(x,min_vis_del_true,color="green",label="del_edges")
plt.plot(x,min_vis_greed_true,color="orange",label="greedy")
plt.legend()

plt.figure()
plt.title("Visibility without pre-processing")
plt.plot(x,min_vis_quick_false,color="red",label="quick")
plt.plot(x,min_vis_red_false,color="blue",label="red_aware")
plt.plot(x,min_vis_del_false,color="green",label="del_edges")
plt.plot(x,min_vis_greed_false,color="orange",label="greedy")
plt.legend()

plt.figure()
plt.title("Quick visibility")
plt.plot(x,moy_vis_quick_true,color="red",label="with pre-processing")
plt.plot(x,moy_vis_quick_false,color="blue",label="without pre-processing")
plt.legend()

plt.figure()
plt.title("Quick + Redundancy Aware visibility")
plt.plot(x,moy_vis_red_true,color="red",label="with pre-processing")
plt.plot(x,moy_vis_red_false,color="blue",label="without pre-processing")
plt.legend()

plt.figure()
plt.title("Quick + Delete covered edges visibility")
plt.plot(x,moy_vis_del_true,color="red",label="with pre-processing")
plt.plot(x,moy_vis_del_false,color="blue",label="without pre-processing")
plt.legend()

plt.figure()
plt.title("Greedy visibility")
plt.plot(x,moy_vis_greed_true,color="red",label="with pre-processing")
plt.plot(x,moy_vis_greed_false,color="blue",label="without pre-processing")
plt.legend()