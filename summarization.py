
import networkx as nx

def DSS_summarization(G,QuasiCliques):
    "compress G into H using the DSS method"
    
    copy_G = G.copy()
    
    def loss_edges(C):
        "return the non edges inside C"
        loss_edges_list = []
        
        for i in range(1,len(C)):
            for j in range(i):
                u = C[i]
                v = C[j]
                
                if not (u in list(G[v])):
                    loss_edges_list.append((u,v))
        
        return loss_edges_list
        
    
    H = nx.Graph()
    H.add_nodes_from(range(len(QuasiCliques)))
    
    for i in range(len(QuasiCliques)):
        H.nodes[i]['super_node_set'] = QuasiCliques[i]
        H.nodes[i]['loss_edges'] = loss_edges(QuasiCliques[i])
        
        for j in range(1,len(QuasiCliques[i])):
            u = QuasiCliques[i][j]
            copy_G.remove_edges_from([(u,QuasiCliques[i][k]) for k in range(j)])
    
    return H, copy_G        # copy_G are the remaining edges



def summary_size(H,remaining_edges):
    "compute the size of the summary"
    
    size = len(remaining_edges.edges)
    for i_Su in H.nodes:
        Su = H.nodes[i_Su]
        size += len(Su['super_node_set'])+len(Su['loss_edges'])
    
    return size