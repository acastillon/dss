import time
import quick
import quick_del_edges as quick_del
import quick_red_aware
import tools
import greedy_qclq_mining as greed_qclq
import summarization
import random as rd

def load_components(filename):

    file = open(filename, "r")
    
    ligne = file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    components = []
    
    while ligne:
        components.append([int(i) for i in ligne.split()])
        ligne = file.readline().rstrip('\n\r')
        file.readline().rstrip('\n\r')
    
    file.close()
    
    return components

def sub_graph_vertices(components,outside_comp):
    
    sommets = set(components[0])
    last_taken = [0 for i in range(len(components))]
    last_taken_outside = 0
    
    for i in range(3000):
        if i%2==0:
            j = (i//2)%(len(components)-1)+1
            ind_j = last_taken[j]
            if ind_j < 36 and ind_j < len(components[j]):
                last_taken[j] += 1
                sommets.add(components[j][ind_j])
        else:
            sommets.add(outside_comp[last_taken_outside])
            last_taken_outside += 1
    
    return sommets
    
    
gamma = 0.9
min_size = 20  

seed = 1000
rd.seed(seed)

G_tot = tools.load_graph("graph_database\\Social_networks\\facebook_combined.txt")
components = load_components("graph_database\\Social_networks\\components_fb.txt")
inside_comp = set()
for c in components:
    inside_comp = inside_comp | set(c)
outside_comp = list(set(G_tot.nodes) - inside_comp)

vertex_list = sub_graph_vertices(components, outside_comp)
G_tot.remove_nodes_from(set(G_tot.nodes) - vertex_list)
vertex_list = list(vertex_list)
rd.shuffle(vertex_list)

bool_append = False

file_vis = open("results\\Social_networks\\fb_vis.txt",("a" if bool_append else "w"))
file = open("results\\Social_networks\\fb.txt",("a" if bool_append else "w"))

if not bool_append:
    file_vis.write("Graph type = fb subgraph, n in [|1,2000|] \n\r")
    file_vis.write("t_baseline_true, comp_baseline_true, t_baseline_false, comp_baseline_false, t_del_edge_true, comp_del_edge_true, t_del_edge_false, comp_del_edges_false, t_red_aware_true, comp_red_aware_true, t_red_aware_false, comp_red_aware_false, t_greedy_true, comp_greedy_true, t_greedy_false, comp_greedy_false, |G|, seed \n\r")
    
    file.write("Graph type = fb subgraph, n dans [|1,200|] \n\r")
    file.write("t_baseline_true, comp_baseline_true, t_baseline_false, comp_baseline_false, t_del_edge_true, comp_del_edge_true, t_del_edge_false, comp_del_edges_false, t_red_aware_true, comp_red_aware_true, t_red_aware_false, comp_red_aware_false, t_greedy_true, comp_greedy_true, t_greedy_false, comp_greedy_false, |G|, seed \n\r")


for j in range(0,1900,25):

    print(j)
    
    G = G_tot.copy()
    G.remove_nodes_from(vertex_list[j:])
    
    G2 = G.copy()
    t0 = time.time()
    tools.reduction(G2,gamma,min_size,edge_red=True)
    t_reduction_true = time.time()-t0
    
    G3 = G.copy()
    t0 = time.time()
    tools.reduction(G3,gamma,min_size,edge_red=False)
    t_reduction_false = time.time()-t0
    
    # Quick ###############################################################
    
    t0 = time.time()
    Qclqs = tools.only_maximal(quick.quick_quasi_cliques(G2,gamma,min_size))
    t_quick_true = t_reduction_true + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_quick_true = summarization.summary_size(H,remaining_edges)
    
    t0 = time.time()
    tot_Qclqs = tools.only_maximal(quick.quick_quasi_cliques(G3,gamma,min_size))
    t_quick_false = t_reduction_false + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_quick_false = summarization.summary_size(H,remaining_edges)
    
    # Quick_del_edges #####################################################
    
    t0 = time.time()
    Qclqs = tools.only_maximal(quick_del.quick_quasi_cliques(G2.copy(),gamma,min_size))
    t_del_edges_true = t_reduction_true + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_del_edges_true = summarization.summary_size(H,remaining_edges)
    vis_del_edges_true = tools.measure_visibility(Qclqs,tot_Qclqs)
    
    t0 = time.time()
    tot_Qclqs = tools.only_maximal(quick_del.quick_quasi_cliques(G3.copy(),gamma,min_size))
    t_del_edges_false = t_reduction_false + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_del_edges_false = summarization.summary_size(H,remaining_edges)
    vis_del_edges_false = tools.measure_visibility(Qclqs,tot_Qclqs)
    
    # Quick_red_aware #####################################################
    
    t0 = time.time()
    Qclqs = tools.only_maximal(quick_red_aware.quick_quasi_cliques(G2,gamma,min_size))
    t_red_aware_true = t_reduction_true + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_red_aware_true = summarization.summary_size(H,remaining_edges)
    vis_red_aware_true = tools.measure_visibility(Qclqs,tot_Qclqs)
    
    t0 = time.time()
    tot_Qclqs = tools.only_maximal(quick_red_aware.quick_quasi_cliques(G3,gamma,min_size))
    t_red_aware_false = t_reduction_false + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_red_aware_false = summarization.summary_size(H,remaining_edges)
    vis_red_aware_false = tools.measure_visibility(Qclqs,tot_Qclqs)
    
    # Greedy ##############################################################
    
    t0 = time.time()
    Qclqs = tools.only_maximal(greed_qclq.greedy_qclq(G2,gamma,min_size))
    t_greedy_true = t_reduction_true + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_greedy_true = summarization.summary_size(H,remaining_edges)
    vis_greedy_true = tools.measure_visibility(Qclqs,tot_Qclqs)
    
    t0 = time.time()
    tot_Qclqs = tools.only_maximal(greed_qclq.greedy_qclq(G3,gamma,min_size))
    t_greedy_false = t_reduction_false + time.time() - t0
    H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
    size_greedy_false = summarization.summary_size(H,remaining_edges)
    vis_greedy_false = tools.measure_visibility(Qclqs,tot_Qclqs)
        
    
    file.write("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \t {8} \t {9} \t {10} \t {11} \t {12} \t {13} \t {14} \t {15} \t {16} \t {17} \n\r".format(t_quick_true, size_quick_true, t_quick_false, size_quick_false, t_del_edges_true, size_del_edges_true, t_del_edges_false, size_del_edges_false, t_red_aware_true, size_red_aware_true, t_red_aware_false, size_red_aware_false, t_greedy_true, size_greedy_true, t_greedy_false, size_greedy_false, len(G.nodes)+len(G.edges), j))
    file_vis.write("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \n\r".format(vis_del_edges_true, vis_del_edges_false, vis_red_aware_true, vis_red_aware_false, vis_greedy_true, vis_greedy_false, len(G.nodes)+len(G.edges), j))


file_vis.close()
file.close()