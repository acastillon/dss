
import numpy as np
import matplotlib.pyplot as plt

def lire_fichier_donnees(filename):
    file = open(filename, 'r')
    
    file.readline().rstrip('\n\r')  #on oublie les 2 premieres lignes
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    nb_ligne = 0
    
    ligne = file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    t_baseline_true = []
    comp_baseline_true = []
    t_red_aware_true = []
    comp_red_aware_true = []
    t_del_edge_true = []
    comp_del_edge_true = []
    t_greedy_true = []
    comp_greedy_true = []
    t_baseline_false = []
    comp_baseline_false = []
    t_red_aware_false = []
    comp_red_aware_false = []
    t_del_edge_false = []
    comp_del_edge_false = []
    t_greedy_false = []
    comp_greedy_false = []
    x = []
    
    while ligne:
        nb_ligne += 1
        l = ligne.split()
        
        t_baseline_true.append(float(l[0]))
        comp_baseline_true.append(float(l[1]))
        t_baseline_false.append(float(l[2]))
        comp_baseline_false.append(float(l[3]))
        t_del_edge_true.append(float(l[4]))
        comp_del_edge_true.append(float(l[5]))
        t_del_edge_false.append(float(l[6]))
        comp_del_edge_false.append(float(l[7]))
        t_red_aware_true.append(float(l[8]))
        comp_red_aware_true.append(float(l[9]))
        t_red_aware_false.append(float(l[10]))
        comp_red_aware_false.append(float(l[11]))
        t_greedy_true.append(float(l[12]))
        comp_greedy_true.append(float(l[13]))
        t_greedy_false.append(float(l[14]))
        comp_greedy_false.append(float(l[15]))
        x.append(float(l[17]))
        
        
        ligne = file.readline().rstrip('\n\r')
        file.readline().rstrip('\n\r')
    
    file.close()
    
    return t_baseline_true, comp_baseline_true, t_baseline_false, comp_baseline_false, t_del_edge_true, comp_del_edge_true, t_del_edge_false, comp_del_edge_false, t_red_aware_true, comp_red_aware_true, t_red_aware_false, comp_red_aware_false, t_greedy_true, comp_greedy_true, t_greedy_false, comp_greedy_false, x



t_baseline_true, comp_baseline_true, t_baseline_false, comp_baseline_false, t_del_edge_true, comp_del_edge_true, t_del_edge_false, comp_del_edge_false, t_red_aware_true, comp_red_aware_true, t_red_aware_false, comp_red_aware_false, t_greedy_true, comp_greedy_true, t_greedy_false, comp_greedy_false, x = lire_fichier_donnees("results\\Social_Networks\\fb.txt")

text_size = 18
avec_legend = False

plt.figure()
plt.title("Runtime with pre-processing (in s)",fontsize=text_size)
plt.plot(x,t_baseline_true,color="red",label="Quick")
plt.plot(x,t_red_aware_true,color="blue",label="Quick_redundancy_aware")
plt.plot(x,t_del_edge_true,color="green",label="Quick_delete_covered_edges")
plt.plot(x,t_greedy_true,color="orange",label="Greedy_quasi_cliques")
#plt.xlabel(chr(946),fontsize=30)
#plt.ylabel("runtime",fontsize=30)
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\all_with_time.png",format='png')

plt.figure()
plt.title("Runtime without pre-processing (in s)",fontsize=text_size)
plt.plot(x,t_baseline_false,color="red",label="quick")
plt.plot(x,t_red_aware_false,color="blue",label="red_aware")
plt.plot(x,t_del_edge_false,color="green",label="del_edges")
plt.plot(x,t_greedy_false,color="orange",label="greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\all_without_time.png",format='png')


plt.figure()
plt.title("Quick runtime (in s)",fontsize=text_size)
plt.plot(x,t_baseline_true,color="red",label="with pre-processing")
plt.plot(x,t_baseline_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\quick_time.png",format='png')

plt.figure()
plt.title("Quick_redundancy_aware runtime (in s)",fontsize=text_size)
plt.plot(x,t_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,t_red_aware_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\red_aware_time.png",format='png')

plt.figure()
plt.title("Quick_delete_covered_edges (in s)",fontsize=text_size)
plt.plot(x,t_del_edge_true,color="red",label="with pre-processing")
plt.plot(x,t_del_edge_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\del_edges_time.png",format='png')

plt.figure()
plt.title("Greedy_quasi_cliques runtime (in s)",fontsize=text_size)
plt.plot(x,t_greedy_true,color="red",label="with pre-processing")
plt.plot(x,t_greedy_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\greedy_time.png",format='png')


plt.figure()
plt.title("Size of the summary with pre-processing",fontsize=text_size)
plt.plot(x,comp_baseline_true,color="red",label="quick")
plt.plot(x,comp_red_aware_true,color="blue",label="red_aware")
plt.plot(x,comp_del_edge_true,color="green",label="del_edges")
plt.plot(x,comp_greedy_true,color="orange",label="greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\all_with_size.png",format='png')

plt.figure()
plt.title("Size of the summary without pre-processing",fontsize=text_size)
plt.plot(x,comp_baseline_false,color="red",label="Quick")
plt.plot(x,comp_red_aware_false,color="blue",label="Redundancy aware")
plt.plot(x,comp_del_edge_false,color="green",label="Delete covered edges")
plt.plot(x,comp_greedy_false,color="orange",label="Greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\all_without_size.png",format='png')


plt.figure()
plt.title("Size of the summary with Quick",fontsize=text_size)
plt.plot(x,comp_baseline_true,color="red",label="with pre-processing")
plt.plot(x,comp_baseline_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\quick_size.png",format='png')

plt.figure()
plt.title("Size of the summary with Quick_redundancy_aware",fontsize=text_size)
plt.plot(x,comp_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,comp_red_aware_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\red_aware_size.png",format='png')

plt.figure()
plt.title("Size of the summary with Quick_delete_covered_edges",fontsize=text_size)
plt.plot(x,comp_del_edge_true,color="red",label="with pre-processing")
plt.plot(x,comp_del_edge_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\del_edges_size.png",format='png')

plt.figure()
plt.title("Size of the summary with Greedy_quasi_cliques",fontsize=text_size)
plt.plot(x,comp_greedy_true,color="red",label="with pre-processing")
plt.plot(x,comp_greedy_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\Social_Networks\\greedy_size.png",format='png')








