
import numpy as np
import matplotlib.pyplot as plt

def lire_fichier_donnees(filename):
    file = open(filename, 'r')
    
    file.readline().rstrip('\n\r')  #on oublie les 2 premieres lignes
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    nb_ligne = 0
    
    ligne = file.readline().rstrip('\n\r')
    file.readline().rstrip('\n\r')
    
    t_baseline_true = 0
    comp_baseline_true = 0
    t_red_aware_true = 0
    comp_red_aware_true = 0
    t_del_edge_true = 0
    comp_del_edge_true = 0
    t_greedy_true = 0
    comp_greedy_true = 0
    t_baseline_false = 0
    comp_baseline_false = 0
    t_red_aware_false = 0
    comp_red_aware_false = 0
    t_del_edge_false = 0
    comp_del_edge_false = 0
    t_greedy_false = 0
    comp_greedy_false = 0
    
    print(filename)
    
    while ligne:
        nb_ligne += 1
        l = ligne.split()
        
        print("ligne:",ligne)
        
        t_baseline_true += float(l[0])
        comp_baseline_true += float(l[1])
        t_baseline_false += float(l[2])
        comp_baseline_false += float(l[3])
        t_del_edge_true += float(l[4])
        comp_del_edge_true += float(l[5])
        t_del_edge_false += float(l[6])
        comp_del_edge_false += float(l[7])
        t_red_aware_true += float(l[8])
        comp_red_aware_true += float(l[9])
        t_red_aware_false += float(l[10])
        comp_red_aware_false += float(l[11])
        t_greedy_true += float(l[12])
        comp_greedy_true += float(l[13])
        t_greedy_false += float(l[14])
        comp_greedy_false += float(l[15])
        
        
        ligne = file.readline().rstrip('\n\r')
        file.readline().rstrip('\n\r')
    
    file.close()
    
    return np.array([t_baseline_true, comp_baseline_true, t_baseline_false, comp_baseline_false, t_del_edge_true, comp_del_edge_true, t_del_edge_false, comp_del_edge_false, t_red_aware_true, comp_red_aware_true, t_red_aware_false, comp_red_aware_false, t_greedy_true, comp_greedy_true, t_greedy_false, comp_greedy_false])/nb_ligne



t_baseline_true = []
comp_baseline_true = []
t_baseline_false = []
comp_baseline_false = []
t_del_edge_true = []
comp_del_edge_true = []
t_del_edge_false = []
comp_del_edge_false = []
t_red_aware_true = []
comp_red_aware_true = []
t_red_aware_false = []
comp_red_aware_false = []
t_greedy_true = []
comp_greedy_true = []
t_greedy_false = []
comp_greedy_false = []

liste_mus = [20,22,24,26,28,30,32,34,37,39,41,43,45,47,49,51,53,55,56,59,61,63,66,68,70,72,74,76,78,80]

for mu in liste_mus:
    l = lire_fichier_donnees("results\\LFR_graphs\\LFR_graphs_300n_"+str(mu)+"mu.txt")
    t_baseline_true.append(l[0])
    comp_baseline_true.append(l[1])
    t_baseline_false.append(l[2])
    comp_baseline_false.append(l[3])
    t_del_edge_true.append(l[4])
    comp_del_edge_true.append(l[5])
    t_del_edge_false.append(l[6])
    comp_del_edge_false.append(l[7])
    t_red_aware_true.append(l[8])
    comp_red_aware_true.append(l[9])
    t_red_aware_false.append(l[10])
    comp_red_aware_false.append(l[11])
    t_greedy_true.append(l[12])
    comp_greedy_true.append(l[13])
    t_greedy_false.append(l[14])
    comp_greedy_false.append(l[15])

#t_del_edge_true = [0.13769824504852296, 0.015675163269042967, 0.015669846534729005, 0.012603402137756348, 0.010821032524108886, 0.00785224437713623, 0.012531638145446777, 0.010931086540222169, 0.006303906440734863, 0.007808184623718262, 0.01097433567047119, 0.012548470497131347, 0.007838010787963867, 0.009438872337341309, 0.009387493133544922, 0.012825989723205566, 0.012174725532531738, 0.012514567375183106, 0.012510108947753906, 0.010909557342529297, 0.012520813941955566, 0.012698698043823241, 0.009384441375732421, 0.009386157989501953, 0.009468150138854981, 0.015651726722717287, 0.01255035400390625, 0.014095330238342285, 0.012532925605773926, 0.011045503616333007]
#t_del_edge_false = [3.873183012008667, 3.459059715270996, 3.185631442070007, 3.251274299621582, 3.2998734951019286, 3.2951504230499267, 3.442635989189148, 3.836528277397156, 3.744323754310608, 3.8696754217147826, 3.881132459640503, 4.0867551326751705, 4.237580943107605, 4.283705806732177, 4.579205393791199, 4.7296847105026245, 4.914470314979553, 5.085553932189941, 5.163970041275024, 5.063702344894409, 5.327744674682617, 5.3884907245635985, 5.404787492752075, 5.554291439056397, 5.639377927780151, 5.728605818748474, 5.7852600574493405, 5.65103280544281, 5.827508163452149, 5.586722898483276]

"""
comp_baseline_true = np.array(comp_baseline_true)/2 + 500
comp_baseline_false = np.array(comp_baseline_false)/2 + 500
comp_red_aware_true = np.array(comp_red_aware_true)-1000
comp_red_aware_false = np.array(comp_red_aware_false)-1000
comp_greedy_true = np.array(comp_greedy_true)+1000
comp_greedy_false = np.array(comp_greedy_false)+1000"""

x = 0.01*np.array(liste_mus)

text_size = 15
avec_legend = False

plt.figure()
plt.title("Runtime with pre-processing (in s)",fontsize=text_size)
plt.plot(x,t_baseline_true,color="red",label="Quick")
plt.plot(x,t_red_aware_true,color="blue",label="Quick_redundancy_aware")
plt.plot(x,t_del_edge_true,color="green",label="Quick_delete_covered_edges")
plt.plot(x,t_greedy_true,color="orange",label="Greedy_quasi_cliques")
#plt.xlabel(chr(946),fontsize=30)
#plt.ylabel("runtime",fontsize=30)
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\all_with_time.png",format='png')

plt.figure()
plt.title("Runtime without pre-processing (in s)",fontsize=text_size)
plt.plot(x,t_baseline_false,color="red",label="quick")
plt.plot(x,t_red_aware_false,color="blue",label="red_aware")
plt.plot(x,t_del_edge_false,color="green",label="del_edges")
plt.plot(x,t_greedy_false,color="orange",label="greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\all_without_time.png",format='png')


plt.figure()
plt.title("Quick runtime (in s)",fontsize=text_size)
plt.plot(x,t_baseline_true,color="red",label="with pre-processing")
plt.plot(x,t_baseline_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\quick_time.png",format='png')

plt.figure()
plt.title("Quick_redundancy_aware runtime (in s)",fontsize=text_size)
plt.plot(x,t_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,t_red_aware_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\red_aware_time.png",format='png')

plt.figure()
plt.title("Quick_delete_covered_edges (in s)",fontsize=text_size)
plt.plot(x,t_del_edge_true,color="red",label="with pre-processing")
plt.plot(x,t_del_edge_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\del_edges_time.png",format='png')

plt.figure()
plt.title("Greedy_quasi_cliques runtime (in s)",fontsize=text_size)
plt.plot(x,t_greedy_true,color="red",label="with pre-processing")
plt.plot(x,t_greedy_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\greedy_time.png",format='png')


plt.figure()
plt.title("Summary size with pre-processing",fontsize=text_size)
plt.plot(x,comp_baseline_true,color="red",label="quick")
plt.plot(x,comp_red_aware_true,color="blue",label="red_aware")
plt.plot(x,comp_del_edge_true,color="green",label="del_edges")
plt.plot(x,comp_greedy_true,color="orange",label="greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\all_with_size.png",format='png')

plt.figure()
plt.title("Summary size without pre-processing",fontsize=text_size)
plt.plot(x,comp_baseline_false,color="red",label="Quick")
plt.plot(x,comp_red_aware_false,color="blue",label="Redundancy aware")
plt.plot(x,comp_del_edge_false,color="green",label="Delete covered edges")
plt.plot(x,comp_greedy_false,color="orange",label="Greedy")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\all_without_size.png",format='png')


plt.figure()
plt.title("Summary size with Quick",fontsize=text_size)
plt.plot(x,comp_baseline_true,color="red",label="with pre-processing")
plt.plot(x,comp_baseline_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\quick_size.png",format='png')

plt.figure()
plt.title("Summary size with Quick_redundancy_aware",fontsize=text_size)
plt.plot(x,comp_red_aware_true,color="red",label="with pre-processing")
plt.plot(x,comp_red_aware_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\red_aware_size.png",format='png')

plt.figure()
plt.title("Summary size with Quick_delete_covered_edges",fontsize=text_size)
plt.plot(x,comp_del_edge_true,color="red",label="with pre-processing")
plt.plot(x,comp_del_edge_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\del_edges_size.png",format='png')

plt.figure()
plt.title("Summary size with Greedy_quasi_cliques",fontsize=text_size)
plt.plot(x,comp_greedy_true,color="red",label="with pre-processing")
plt.plot(x,comp_greedy_false,color="blue",label="without pre-processing")
if avec_legend:
    plt.legend()
plt.savefig("figures\\LFR_graphs\\greedy_size.png",format='png')





