# DSS

Comparision of several quasi-clique mining algorithms and their application to the DSS method. This code uses the modules networkx, random, time and matplotlib.pyplot.

The different quasi-clique mining algorithms used are:

    - Quick (Effective Pruning Techniques for Mining Quasi-Cliques 2008) implemented in the file quick.py

    - Quick_redundancy_aware (same as before and adding a method from Redundancy Aware Maximal Cliques 2013) implemented in the file quick_red_aware.py

    - Quick_delete_covered_edges, implemented in the file quick_del_edges.py

    - A greedy quasi-clique mining algorithm implemented in greedy_qclq_mining.py

These algorithms are compared trough several aspects (runtime/compression rates/visibility), each time with and without the pre-processing algorithm (the reduction procedure of tools.py).

Three types of graph have been used:
    - community graphs (generated with the file community_graph.py)

    - LFR graphs (the dataset used is available here: https://zenodo.org/record/4450167?fbclid=IwAR1abb2_3qotisogJW9i_F_6RL-Cjd_czgljmh-tjONXOOzgMVfHnLWx0Ac#.Yi4FcDXjK3C and the setB folder must be placed in graph_database/LFR_graphs)

    - social networks (the dataset used is available here: https://snap.stanford.edu/data/ and must be placed in graph_database/Social_networks)

To compare the algorithms run either (can take several hours):
    - test_community_graphs.py
    - test_LFR_graphs.py
    - test_social_networks.py
The results will be saved in the results folder.

To output the results run either:
    - results_community_graphs.py (or visibility_community_graphs.py)
    - results_LFR_graphs.py (or visibility_LFR_graphs.py)
    - results_social_networks.py (or visibility_social_networks_graphs.py)

