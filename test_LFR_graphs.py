
import time
import quick
import quick_del_edges as quick_del
import quick_red_aware
import tools
import greedy_qclq_mining as greed_qclq
import summarization
from os import listdir


def load_communities(path2file):
    "load the communities of a LFR graph"

    file = open(path2file)
    comm = []
    
    S = ''
    
    line = file.readline().rstrip('\n\r')
    while line:
        S += line
        line = file.readline().rstrip('\n\r')
    
    true = True             # these keywords are defined in the .json file and
    false = False           # must be introduced for the eval
    null = None
    
    dic = eval(S)
    
    comm = (dic["clu2elm_dict"]).values()
    
    file.close()
    
    return comm


gamma = 0.9
min_size = 14

taille_graphe = 300

file_path_1 = "graph_database\\LFR_graphs\\setB\\"+str(taille_graphe)+"n"

mu_list = [int(f[:-2]) for f in listdir(file_path_1)]

for mu in mu_list:

    file = open("results\\LFR_graphs\\LFR_graphs_"+str(taille_graphe)+"n_"+str(mu)+"mu.txt","w")
    file.write("Graph type = LFR, n = "+str(taille_graphe)+", mu = "+str(mu)+"/100 \n\r")
    file.write("t_quick_true, size_quick_true, t_quick_false, size_quick_false, t_del_edges_true, size_del_edges_true, t_del_edges_false, size_del_edges_false, t_red_aware_true, size_red_aware_true, t_red_aware_false, size_red_aware_false, t_greedy_true, size_greedy_true, t_greedy_false, size_greedy_false, |G|, seed \n\r")
    
    file_vis = open("results\\LFR_graphs\\LFR_graph_vis_"+str(taille_graphe)+"n_"+str(mu)+"mu.txt","w")
    file_vis.write("type de graphe = LFR, n = "+str(taille_graphe)+", mu = "+str(mu)+"/100 \n\r")
    file_vis.write("vis_del_edges_true, vis_del_edges_false, vis_red_aware_true, vis_red_aware_false, vis_greedy_true, vis_greedy_false, |G|, seed \n\r")
    
    file_path_2 = file_path_1+"\\"+str(mu)+"mu"
    
    id_list = [f[6:-4] for f in listdir(file_path_2) if f[:5]=='graph'] #on ne garde que les numeros des .txt
    
    for id_graphs in id_list:
        
        print(mu,id_graphs)
        
        G = tools.load_graph(file_path_2+"\\graph_"+id_graphs+".txt")
    
        G2 = G.copy()
        t0 = time.time()
        tools.reduction(G2,gamma,min_size,edge_red=True)
        t_reduction_true = time.time()-t0
        
        G3 = G.copy()
        t0 = time.time()
        tools.reduction(G3,gamma,min_size,edge_red=False)
        t_reduction_false = time.time()-t0
        
        # Quick ###############################################################
        
        t0 = time.time()
        Qclqs = tools.only_maximal(quick.quick_quasi_cliques(G2,gamma,min_size))
        t_quick_true = t_reduction_true + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_quick_true = summarization.summary_size(H,remaining_edges)
        
        t0 = time.time()
        tot_Qclqs = tools.only_maximal(quick.quick_quasi_cliques(G3,gamma,min_size))
        t_quick_false = t_reduction_false + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_quick_false = summarization.summary_size(H,remaining_edges)
        
        # Quick_del_edges #####################################################
        
        t0 = time.time()
        Qclqs = tools.only_maximal(quick_del.quick_quasi_cliques(G2.copy(),gamma,min_size))
        t_del_edges_true = t_reduction_true + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_del_edges_true = summarization.summary_size(H,remaining_edges)
        vis_del_edges_true = tools.measure_visibility(Qclqs,tot_Qclqs)
        
        t0 = time.time()
        tot_Qclqs = tools.only_maximal(quick_del.quick_quasi_cliques(G3.copy(),gamma,min_size))
        t_del_edges_false = t_reduction_false + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_del_edges_false = summarization.summary_size(H,remaining_edges)
        vis_del_edges_false = tools.measure_visibility(Qclqs,tot_Qclqs)
        
        # Quick_red_aware #####################################################
        
        t0 = time.time()
        Qclqs = tools.only_maximal(quick_red_aware.quick_quasi_cliques(G2,gamma,min_size))
        t_red_aware_true = t_reduction_true + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_red_aware_true = summarization.summary_size(H,remaining_edges)
        vis_red_aware_true = tools.measure_visibility(Qclqs,tot_Qclqs)
        
        t0 = time.time()
        tot_Qclqs = tools.only_maximal(quick_red_aware.quick_quasi_cliques(G3,gamma,min_size))
        t_red_aware_false = t_reduction_false + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_red_aware_false = summarization.summary_size(H,remaining_edges)
        vis_red_aware_false = tools.measure_visibility(Qclqs,tot_Qclqs)
        
        # Greedy ##############################################################
        
        t0 = time.time()
        Qclqs = tools.only_maximal(greed_qclq.greedy_qclq(G2,gamma,min_size))
        t_greedy_true = t_reduction_true + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_greedy_true = summarization.summary_size(H,remaining_edges)
        vis_greedy_true = tools.measure_visibility(Qclqs,tot_Qclqs)
        
        t0 = time.time()
        tot_Qclqs = tools.only_maximal(greed_qclq.greedy_qclq(G3,gamma,min_size))
        t_greedy_false = t_reduction_false + time.time() - t0
        H,remaining_edges = summarization.DSS_summarization(G,Qclqs)
        size_greedy_false = summarization.summary_size(H,remaining_edges)
        vis_greedy_false = tools.measure_visibility(Qclqs,tot_Qclqs)

        
        file.write("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \t {8} \t {9} \t {10} \t {11} \t {12} \t {13} \t {14} \t {15} \t {16} \t {17} \n\r".format(t_quick_true, size_quick_true, t_quick_false, size_quick_false, t_del_edges_true, size_del_edges_true, t_del_edges_false, size_del_edges_false, t_red_aware_true, size_red_aware_true, t_red_aware_false, size_red_aware_false, t_greedy_true, size_greedy_true, t_greedy_false, size_greedy_false, len(G.nodes)+len(G.edges), id_graphs))
        file_vis.write("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \n\r".format(vis_del_edges_true, vis_del_edges_false, vis_red_aware_true, vis_red_aware_false, vis_greedy_true, vis_greedy_false, len(G.nodes)+len(G.edges), id_graphs))



    file.close()
    file_vis.close()